"use strict";
var path = require("path");
var S3VideoKeyParser = (function () {
    function S3VideoKeyParser() {
    }
    S3VideoKeyParser.decode = function (key, splitChar) {
        if (splitChar === void 0) { splitChar = '/'; }
        var extension = '';
        var extIndex = key.indexOf('.');
        if (extIndex > 0) {
            extension = key.substr(extIndex);
        }
        key = key.replace(extension, '');
        var splitKey = key.split(splitChar);
        if (splitKey.length !== 4) {
            throw 'wrong video key format!';
        }
        var appId = splitKey[0];
        var roomId = parseInt(splitKey[1]);
        var callUuid = splitKey[2];
        var uuid = splitKey[3];
        return { appId: appId, roomId: roomId, callUuid: callUuid, uuid: uuid, extension: extension };
    };
    S3VideoKeyParser.encode = function (vd, splitChar, useExtension) {
        if (splitChar === void 0) { splitChar = '/'; }
        if (useExtension === void 0) { useExtension = true; }
        return "" + vd.appId + splitChar + vd.roomId + splitChar + vd.callUuid + splitChar + vd.uuid + (useExtension ? vd.extension : '');
    };
    S3VideoKeyParser.decodeKurentoFilename = function (srcPath) {
        var baseName = path.basename(srcPath);
        return this.decode(baseName, '!');
    };
    return S3VideoKeyParser;
}());
exports.S3VideoKeyParser = S3VideoKeyParser;
function getHostUrl(appId) {
    switch (appId) {
        case 'yack-dev':
            return 'dev.yack.net';
        case 'yack-stage':
            return 'stage.yack.net';
        case 'yack-prod':
            return 'my.yack.net';
        case 'yack-systest':
            return 'systest.yack.net';
        case 'yack-local-mk':
            return 'localhost';
        default:
            return 'dev.yack.net';
    }
}
exports.getHostUrl = getHostUrl;
//# sourceMappingURL=s3-video-key-parser.js.map
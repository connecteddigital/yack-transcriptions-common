export interface IVideoData {
    appId: string;
    roomId: number;
    callUuid: string;
    uuid: string;
    extension: string;
}
export declare class S3VideoKeyParser {
    static decode(key: string, splitChar?: string): IVideoData;
    static encode(vd: IVideoData, splitChar?: string, useExtension?: boolean): string;
    static decodeKurentoFilename(srcPath: string): IVideoData;
}
export declare function getHostUrl(appId: string): "dev.yack.net" | "stage.yack.net" | "my.yack.net" | "systest.yack.net" | "localhost";

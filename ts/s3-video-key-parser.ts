import * as path from 'path';
export interface IVideoData {
  appId: string;
  roomId: number;
  callUuid: string;
  uuid: string;
  extension: string;
}

export class S3VideoKeyParser {
  // appId/roomId/callUuid/uuid.webm
  static decode(key: string, splitChar = '/'): IVideoData {
    let extension = '';
    const extIndex = key.indexOf('.');
    if(extIndex > 0) {
      extension = key.substr(extIndex);
    }
    key = key.replace(extension, '');

    const splitKey = key.split(splitChar);
    if (splitKey.length !== 4) {
      throw 'wrong video key format!'
    }

    const appId = splitKey[0];
    const roomId = parseInt(splitKey[1]);
    const callUuid = splitKey[2];
    const uuid = splitKey[3];

    return {appId, roomId, callUuid, uuid, extension}
  }

  static encode(vd: IVideoData, splitChar = '/', useExtension = true) {
    return `${vd.appId}${splitChar}${vd.roomId}${splitChar}${vd.callUuid}${splitChar}${vd.uuid}${useExtension ? vd.extension : ''}`;
  }

  // file:///kurento-recorded/appId-roomId-callUuid-uuid.webm
  static decodeKurentoFilename(srcPath: string) {
    const baseName = path.basename(srcPath);
    return this.decode(baseName, '!');
  }
}

export function getHostUrl(appId: string) {
  switch (appId) {
    case 'yack-dev':
      return 'dev.yack.net';
    case 'yack-stage':
      return 'stage.yack.net';
    case 'yack-prod':
      return 'my.yack.net';
    case 'yack-systest':
      return 'systest.yack.net';
    case 'yack-local-mk':
      return 'localhost';
    default:
      return 'dev.yack.net';
  }
}
import {getHostUrl, S3VideoKeyParser} from './s3-video-key-parser';
var chai = require('chai');
chai.config.includeStack = true;


describe('S3VideoKeyParser', () => {
  it('should decode kurento filename', () => {
    chai.expect(S3VideoKeyParser.decodeKurentoFilename('yack-local-mk!3!6753fd59-1fa0-409a-aca5-5f039852b640!9a6d1597-5c1a-406a-bc5a-23f2a1c0997c.webm'))
      .to.deep.equal({
      appId: 'yack-local-mk',
      roomId: 3,
      callUuid: '6753fd59-1fa0-409a-aca5-5f039852b640',
      uuid: '9a6d1597-5c1a-406a-bc5a-23f2a1c0997c',
      extension: '.webm'
    });
  });

  it('should encode', () => {
    chai.expect(S3VideoKeyParser.encode({
      appId: 'yack-local-mk',
      roomId: 3,
      callUuid: '6753fd59-1fa0-409a-aca5-5f039852b640',
      uuid: '9a6d1597-5c1a-406a-bc5a-23f2a1c0997c',
      extension: '.webm'
    })).to.deep.equal('yack-local-mk/3/6753fd59-1fa0-409a-aca5-5f039852b640/9a6d1597-5c1a-406a-bc5a-23f2a1c0997c.webm');
  });

  it('should decode .webm.vtt', () => {
    chai.expect(S3VideoKeyParser.decode('yack-dev/286/3d695dcf-7547-4db4-8cee-88431e125002/11da0f07-61ad-45f3-9203-e2069934fa9b.webm.vtt'))
      .to.deep.equal({
      appId: 'yack-dev',
      roomId: 286,
      callUuid: '3d695dcf-7547-4db4-8cee-88431e125002',
      uuid: '11da0f07-61ad-45f3-9203-e2069934fa9b',
      extension: '.webm.vtt'
    });
  });


  it('should decode webm', () => {
    chai.expect(S3VideoKeyParser.decode('yack-dev/286/3d695dcf-7547-4db4-8cee-88431e125002/11da0f07-61ad-45f3-9203-e2069934fa9b.webm'))
      .to.deep.equal({
      appId: 'yack-dev',
      roomId: 286,
      callUuid: '3d695dcf-7547-4db4-8cee-88431e125002',
      uuid: '11da0f07-61ad-45f3-9203-e2069934fa9b',
      extension: '.webm'
    });
  });

  it('should get dev server url', () => {
    chai.expect(getHostUrl('yack-dev'))
      .to.equal('dev.yack.net');
  })
});